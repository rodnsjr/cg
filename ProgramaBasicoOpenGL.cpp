#include <iostream>
#include <math.h>
#include <fstream>
#include <windows.h>
#include <ctime>
#include <gl/glut.h>

using namespace std;

#define ANG2RAD 3.14159265358979323846/180.0

class Ponto
{
public:
    float x,y,z;
    char tipo;

    Ponto() {
    };
    Ponto(float x,float y,float z){
    	this->x=x; this->y=y; this->z=z;
	}
    Ponto(float x,float y,float z, char tipo) {
        this->x=x; this->y=y; this->z=z;this->tipo=tipo;
    }

Ponto operator+(const Ponto &v) {

	Ponto res;

	res.x = x + v.x;
	res.y = y + v.y;
	res.z = z + v.z;

	return(res);
}

Ponto operator-(const Ponto &v) {

	Ponto res;

	res.x = x - v.x;
	res.y = y - v.y;
	res.z = z - v.z;

	return(res);
}

Ponto operator-(void) {

	Ponto res;

	res.x = -x;
	res.y = -y;
	res.z = -z;

	return(res);
}

// cross product
Ponto operator*(Ponto &v) {

	Ponto res;

	res.x = y * v.z - z * v.y;
	res.y = z * v.x - x * v.z;
	res.z = x * v.y - y * v.x;

	return (res);
}

Ponto operator*(float t) {

	Ponto res;

	res.x = x * t;
	res.y = y * t;
	res.z = z * t;

	return (res);
}


Ponto operator/(float t) {

	Ponto res;

	res.x = x / t;
	res.y = y / t;
	res.z = z / t;

	return (res);
}

Ponto copy(const Ponto &v) {

	x = v.x;
	y = v.y;
	z = v.z;
}

float length() {

	return((float)sqrt(x*x + y*y + z*z));
}

void normalize() {

	float len;

	len = length();
	if (len) {
		x /= len;;
		y /= len;
		z /= len;
	}
}


float innerProduct(Ponto &v) {

	return (x * v.x + y * v.y + z * v.z);
}
};

class Plano
{
private:
    Ponto normal, ponto;
	float d;
public:
    void setPontos( Ponto &p1,  Ponto &p2,  Ponto &p3) {
        Ponto aux1, aux2;

        aux1 = p1 - p2;
        aux2 = p3 - p2;

        normal = p2 * p1;

        normal.normalize();
        ponto.copy(p2);
        d = -(normal.innerProduct(ponto));
    }

    float distance(Ponto &ponto1) {
        return (d + normal.innerProduct(ponto1));
    }
};

class Frustum
{
public:
    Plano pl[6];
	Ponto ntl,ntr,nbl,nbr,ftl,ftr,fbl,fbr;
	float nearD, farD, rratio, angle,tang;
	float nw,nh,fw,fh;

int sphereInFrustum(Ponto &ponto, float raio) {

	int result = 0;
	float distance;

	for(int i=0; i < 6; i++) {
		distance = pl[i].distance(ponto);
		if (distance < -raio){
            //OUTSIDE
            return -1;
		}
		else if (distance < raio){
            //INTERSECT
            result = 1;
		}
	}
	return(result);

}

void setCamDef(Ponto &p, Ponto &l, Ponto &u) {

	Ponto dir,nc,fc,X,Y,Z;

	Z = p - l;
	Z.normalize();

	X = u * Z;
	X.normalize();

	Y = Z * X;

	nc = p - Z * nearD;
	fc = p - Z * farD;

	ntl = nc + Y * nh - X * nw;
	ntr = nc + Y * nh + X * nw;
	nbl = nc - Y * nh - X * nw;
	nbr = nc - Y * nh + X * nw;

	ftl = fc + Y * fh - X * fw;
	ftr = fc + Y * fh + X * fw;
	fbl = fc - Y * fh - X * fw;
	fbr = fc - Y * fh + X * fw;

    pl[0].setPontos(ntr,ntl,ftl);
	pl[1].setPontos(nbl,nbr,fbr);
	pl[2].setPontos(ntl,nbl,fbl);
	pl[3].setPontos(nbr,ntr,fbr);
	pl[4].setPontos(ntl,ntr,nbr);
	pl[5].setPontos(ftr,ftl,fbl);
}

void setCamInternals(float angle, float rratio, float nearD, float farD) {

	// store the information
	this->rratio = rratio;
	this->angle = angle;
	this->nearD = nearD;
	this->farD = farD;

	// compute width and height of the near and far plane sections
	tang = (float)tan(ANG2RAD * angle * 0.5) ;
	nh = nearD * tang;
	nw = nh * rratio;
	fh = farD  * tang;
	fw = fh * rratio;
}
};

Ponto *Esferas;
Frustum frustum;
int QtdTotal=0;
int angulo=0;

int minDistX=-200, maxDistX=200, minDistY=-200, maxDistY=200, minDistZ=-200, maxDistZ=200;

int frame=0, timebase=0;
Ponto p(0,0,-120), l(0,0,0), u(0,1,0);
float nearP = 30.0f, farP = 100.0f;
float angle = 60;
float rratio=1;

Ponto lodThreshold(0,0,30);

bool lod = false;
bool frustumEnable = true;
// **********************************************************************
//  void init(void)
//  Inicializa os par?metros globais de OpenGL
//
// **********************************************************************
void init(void)
{
    srand(time(NULL));
    //Pequeno
    //string Nome = "4cpt.xyz";
    //Medio
    //string Nome = "TIM_waterbox.xyz";
	//Grande
    string Nome = "HIV_waterbox.xyz";

    // Define a cor do fundo da tela (AZUL)
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    ifstream arq;
    arq.open(Nome.c_str(), ios::in);
    arq >> QtdTotal;
    Esferas = new Ponto[QtdTotal];
    float x,y,z;
    char tipo;
    for (int i=0; i< QtdTotal; i++)
    {
        arq >> tipo;
        arq >> x;
        arq >> y;
        arq >> z;
        Esferas [i] = Ponto(x,y,z,tipo);
    }

}

// **********************************************************************
//  void reshape( int w, int h )
//  trata o redimensionamento da janela OpenGL
//
// **********************************************************************
void reshape( int w, int h )
{
    // Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // Define a ?rea a ser ocupada pela ?rea OpenGL dentro da Janela
    glViewport(0, 0, w, h);

    // Define os limites l?gicos da ?rea OpenGL dentro da Janela
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glOrtho(minDistX,maxDistX,minDistY,maxDistY,minDistZ,maxDistZ);

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	rratio = w * 1.0/ h;

	// Set the correct perspective.
	gluPerspective(angle,rratio,nearP,farP);

   	frustum.setCamInternals(angle,rratio,nearP,farP);

}

bool checkLod(Ponto p1){
    if (lod)
        return p1.z > lodThreshold.z;
    return true;
}

bool checkFrustum(Ponto p1){
    if (frustumEnable){
        //-1 == outside of frustum
        return frustum.sphereInFrustum(p1,0.4) != -1;
    }
    return true;
}

void DesenhaEsfera(Ponto p1){
    glPushMatrix();
            glTranslatef(p1.x,p1.y,p1.z);
            glutWireSphere(3,6,6);
    glPopMatrix();
}

void DesenhaEsferaLowRes(Ponto p1){
    glPushMatrix();
            glTranslatef(p1.x,p1.y,p1.z);
            glutWireSphere(3,2,2);
    glPopMatrix();
}

// **********************************************************************
//  void display( void )
//
// **********************************************************************
void VerificarEDesenhar(Ponto p1)
{
    if (checkFrustum(p1)) {
        //Esferas dentro do campo de vis?o do frustum, por?m muito distantes
        //do campo de detalhes, ser?o renderizadas com um menor numero de poligonos
        if (checkLod(p1)){
            DesenhaEsfera(p1);
        }else{
            DesenhaEsferaLowRes(p1);
        }
    //Caso o frustum esteja desabilitado
    }
}
// **********************************************************************
//  void display( void )
//
// **********************************************************************
void GeraCor(char tipo)
{
    switch(tipo)
    {
        case 'N': glColor3f(1,0,0);
        break;
        case 'C': glColor3f(1,1,0);
        break;
        case '0': glColor3f(1,0,1);
        break;
    }
    //glColor3ub(rand()%256, rand()%256, rand()%256);
}

// **********************************************************************
//  void display( void )
//
// **********************************************************************
void DesenhaEsfera(int i)
{
    GeraCor(Esferas[i].tipo);
    VerificarEDesenhar(Esferas[i]);
}
// **********************************************************************
//  void display( void )
//
// **********************************************************************

void display( void )
{
    char title[20];
    float fps,time;
	// Limpa a tela coma cor de fundo
	glClear(GL_COLOR_BUFFER_BIT);

    // Define os limites l?gicos da ?rea OpenGL dentro da Janela
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glOrtho(minDistX,maxDistX,minDistY,maxDistY,minDistZ,maxDistZ);
    gluLookAt(p.x, p.y, p.z, l.x, l.y, l.z, u.x,u.y, u.z);
    frustum.setCamDef(p,l,u);

    glRotatef(angulo,0,1,0);
    for(int i=0; i<QtdTotal; i++)
    {
        DesenhaEsfera(i);
    }

    frame++;
	time=glutGet(GLUT_ELAPSED_TIME);
	if (time - timebase > 1000) {
		fps = frame*1000.0/(time-timebase);
		timebase = time;
		frame = 0;
		sprintf(title,"FPS: %8.2f", fps);
		glutSetWindowTitle(title);
	}

	glutSwapBuffers();
}


// **********************************************************************
//  void keyboard ( unsigned char key, int x, int y )
//
// **********************************************************************

void keyboard ( unsigned char key, int x, int y )
{
	switch ( key )
	{
		case 27:        // Termina o programa qdo
			exit ( 0 );   // a tecla ESC for pressionada
			break;
        case 'a':
            lod = true;
            frustumEnable=true;
            angulo++;
            glutPostRedisplay();
            break;
        case 'f':
            frustumEnable=true;
            angulo++;
            glutPostRedisplay();
            break;
        case 'l':
            lod = true;
            angulo++;
            glutPostRedisplay();
            break;
        case ' ':
            frustumEnable=false;
		    lod=false;
            angulo++;
            glutPostRedisplay();
            break;
		default:
			break;
	}
}


// **********************************************************************
//  void arrow_keys ( int a_keys, int x, int y )
//
//
// **********************************************************************
void arrow_keys ( int a_keys, int x, int y )
{
	switch ( a_keys )
	{
		case GLUT_KEY_UP:       // Se pressionar UP
			glutFullScreen ( ); // Vai para Full Screen
			break;
	    case GLUT_KEY_DOWN:     // Se pressionar UP
								// Reposiciona a janela
            glutPositionWindow (50,50);
			glutReshapeWindow ( 700, 500 );
			break;
		default:
			break;
	}
}

// **********************************************************************
//  void main ( int argc, char** argv )
//
//
// **********************************************************************
int  main ( int argc, char** argv )
{
    glutInit            ( &argc, argv );
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB );
    glutInitWindowPosition (0,0);

    // Define o tamanho inicial da janela grafica do programa
    glutInitWindowSize  ( 650, 500);

    // Cria a janela na tela, definindo o nome da
    // que aparecera na barra de t?tulo da janela.
    glutCreateWindow    ( "Primeiro Programa em OpenGL" );

    // executa algumas inicializa??es
    init ();


    // Define que o tratador de evento para
    // o redesenho da tela. A funcao "display"
    // ser? chamada automaticamente quando
    // for necess?rio redesenhar a janela
    glutDisplayFunc ( display );

    // Define que o tratador de evento para
    // o redimensionamento da janela. A funcao "reshape"
    // ser? chamada automaticamente quando
    // o usu?rio alterar o tamanho da janela
    glutReshapeFunc ( reshape );

    // Define que o tratador de evento para
    // as teclas. A funcao "keyboard"
    // ser? chamada automaticamente sempre
    // o usu?rio pressionar uma tecla comum
    glutKeyboardFunc ( keyboard );

    // Define que o tratador de evento para
    // as teclas especiais(F1, F2,... ALT-A,
    // ALT-B, Teclas de Seta, ...).
    // A funcao "arrow_keys" ser? chamada
    // automaticamente sempre o usu?rio
    // pressionar uma tecla especial
    glutSpecialFunc ( arrow_keys );

    // inicia o tratamento dos eventos
    glutMainLoop ( );

    return 0;
}
