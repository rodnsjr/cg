#Ambientes
1. Caso com o arquivo ('4cpt.xyz') onde o numero de esferas é de 1856, a taxa de frames é limitada a 30, e continua em 30.
2. Caso com o arquivo('TIM_WATERBOX.xyz') o numero de esferas é de 26110, e a taxa de frames sem a presença de nenhum algoritmo de culling, é de 6 a 7fps.
3. Caso com o arquivo('HIV_WATERBOX.xyz') o numero de esferas é de 95679, e a taxa de frames é de 1.5fps.

#Frustum
Extração de Planos - Abordagem Geométrica
![culling.gif](https://bitbucket.org/repo/xkpjrX/images/1856759993-culling.gif)

![vfpoints2.gif](https://bitbucket.org/repo/xkpjrX/images/198539411-vfpoints2.gif)

##Parâmetros:
```
#!c++

Ponto p(0,0,-120), l(0,0,0), u(0,1,0);
float nearP = 30.0f, farP = 100.0f;
float angle = 60;
float rratio=1;

```

Algoritmo baseado na implementação disponibilizada pelo site [Lighthouse](http://www.lighthouse3d.com/tutorials/view-frustum-culling/)

O tamanho do Frustum ficou padrão para todos os casos:
Sendo um espaço pequeno, o FPS continua o mesmo para todos os casos.

![frustum.png](https://bitbucket.org/repo/xkpjrX/images/705607755-frustum.png)


#Level of Detail
Extração de Polígonos, abordagem por distância do campo de visão, utilizando um campo como threshold:

```
#!c++

Ponto lodThreshold(0,0,30);

```

Numero de vertices menor caso estejam num campo de visão muito afastado, nesse caso foi utilizado a função do próprio glut.

```
#!c++

glutWireSphere(3,2,2);

```
1. Caso com ou sem o Lod o FPS médio continua de 30.
2. Caso com Lod o FPS médio e de 11.5, o custo de memória é cerca de 4,5mb. No mesmo cenário, sem a aplicação de LOD o custo passa a ser de 4,3mb, porém o FPS passa a cair para 5.7.
3. Caso com Lod o FPS médio é de 3,30 e o custo de memória é de cerca de 13,3mb. Já no mesmo cenário sem a aplicação de LoD o FPS é de cerca de 1.5.