// **********************************************************************
// PUCRS/FACIN
// COMPUTA��O GR�FICA
//
// Programa b�sico para criar aplica��es em OpenGL
//
// Marcio Sarroglia Pinho
// pinho@inf.pucrs.br
// **********************************************************************

#include <iostream>
#include <math.h>
#include <fstream>
#include <windows.h>
#include <ctime>
#include <gl/glut.h>

using namespace std;

#define ANG2RAD 3.14159265358979323846/180.0

class Ponto
{
public:
    float x,y,z;
    char tipo;

    Ponto() {
    };
    Ponto(float x,float y,float z, char tipo) {
        this->x=x; this->y=y; this->z=z;this->tipo=tipo;
    }

    Ponto Ponto::operator+(Ponto &v) {

	Ponto res;

	res.x = x + v.x;
	res.y = y + v.y;
	res.z = z + v.z;

	return(res);
}

operator-(const Ponto &v) {

	Ponto res;

	res.x = x - v.x;
	res.y = y - v.y;
	res.z = z - v.z;

	return(res);
}

operator-(void) {

	Ponto res;

	res.x = -x;
	res.y = -y;
	res.z = -z;

	return(res);
}

// cross product
operator*(Ponto &v) {

	Ponto res;

	res.x = y * v.z - z * v.y;
	res.y = z * v.x - x * v.z;
	res.z = x * v.y - y * v.x;

	return (res);
}

operator*(float t) {

	Ponto res;

	res.x = x * t;
	res.y = y * t;
	res.z = z * t;

	return (res);
}


operator/(float t) {

	Ponto res;

	res.x = x / t;
	res.y = y / t;
	res.z = z / t;

	return (res);
}



float length() {

	return((float)sqrt(x*x + y*y + z*z));
}

void normalize() {

	float len;

	len = length();
	if (len) {
		x /= len;;
		y /= len;
		z /= len;
	}
}


float innerProduct(Ponto &v) {

	return (x * v.x + y * v.y + z * v.z);
}
};

class Plano
{
private:
    Ponto normal, ponto;
	float d;
public:
    void setPontos( Ponto &p1,  Ponto &p2,  Ponto &p3) {
        Ponto aux1, aux2;

        aux1 = p1 - p2;
        aux2 = p3 - p2;

        normal = p2 * p1;

        normal.normalize();
        ponto.copy(v2);
        d = -(normal.innerProduct(ponto));
    }

    float distance(Ponto &p) {
        return (d + normal.innerProduct(p));
    }
};

class Frustrum
{
public:
    Plano pl[6];
	Ponto ntl,ntr,nbl,nbr,ftl,ftr,fbl,fbr;
	float nearD, farD, ratio, angle,tang;
	float nw,nh,fw,fh;

int sphereInFrustum(Ponto &p, float raio) {

	int result = 0;
	float distance;

	for(int i=0; i < 6; i++) {
		distance = pl[i].distance(p);
		if (distance < -raio)
			return -1;
		else if (distance < raio)
			result = 1;
	}
	return(result);

}

void setCamDef(Ponto &p, Ponto &l, Ponto &u) {

	Ponto dir,nc,fc,X,Y,Z;

	Z = p - l;
	Z.normalize();

	X = u * Z;
	X.normalize();

	Y = Z * X;

	nc = p - Z * nearD;
	fc = p - Z * farD;

	ntl = nc + Y * nh - X * nw;
	ntr = nc + Y * nh + X * nw;
	nbl = nc - Y * nh - X * nw;
	nbr = nc - Y * nh + X * nw;

	ftl = fc + Y * fh - X * fw;
	ftr = fc + Y * fh + X * fw;
	fbl = fc - Y * fh - X * fw;
	fbr = fc - Y * fh + X * fw;

    pl[0].set3Points(ntr,ntl,ftl);
	pl[1].set3Points(nbl,nbr,fbr);
	pl[2].set3Points(ntl,nbl,fbl);
	pl[3].set3Points(nbr,ntr,fbr);
	pl[4].set3Points(ntl,ntr,nbr);
	pl[5].set3Points(ftr,ftl,fbl);
}

void setCamInternals(float angle, float ratio, float nearD, float farD) {

	// store the information
	this->ratio = ratio;
	this->angle = angle;
	this->nearD = nearD;
	this->farD = farD;

	// compute width and height of the near and far plane sections
	tang = (float)tan(ANG2RAD * angle * 0.5) ;
	nh = nearD * tang;
	nw = nh * ratio;
	fh = farD  * tang;
	fw = fh * ratio;
}
};

Ponto *Esferas;
Frustrum frustrum;
int QtdTotal=0;
int angulo=0;

// **********************************************************************
//  void init(void)
//  Inicializa os par�metros globais de OpenGL
//
// **********************************************************************
void init(void)
{
    srand(time(NULL));
    string Nome = "4cpt.xyz";
    //string Nome = "HIV_waterbox.xyz";

	// Define a cor do fundo da tela (AZUL)
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    ifstream arq;
    arq.open(Nome.c_str(), ios::in);
    arq >> QtdTotal;
    Esferas = new Ponto[QtdTotal];
    float x,y,z;
    char tipo;
    for (int i=0; i< QtdTotal; i++)
    {
        arq >> tipo;
        arq >> x;
        arq >> y;
        arq >> z;
        Esferas [i] = Ponto(x,y,z,tipo);
    }

}

// **********************************************************************
//  void reshape( int w, int h )
//  trata o redimensionamento da janela OpenGL
//
// **********************************************************************
void reshape( int w, int h )
{
    // Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // Define a �rea a ser ocupada pela �rea OpenGL dentro da Janela
    glViewport(0, 0, w, h);

    // Define os limites l�gicos da �rea OpenGL dentro da Janela
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glOrtho(-50,50,-50,50,-300,300);

   	frustum.setCamInternals(angle,ratio,nearP,farP);

}
// **********************************************************************
//  void display( void )
//
// **********************************************************************
void DesenhaEsfera(Ponto p)
{
    if (frustum.sphereInFrustum(p,0.5) != 0) {

        glPushMatrix();
            glTranslatef(p.x,p.y,p.z);
            glutWireSphere(5,10,10);
        glPopMatrix();

    }
}
// **********************************************************************
//  void display( void )
//
// **********************************************************************
void GeraCor(char tipo)
{
    switch(tipo)
    {
        case 'N': glColor3f(1,0,0);
        break;
        case 'C': glColor3f(1,1,0);
        break;
        case '0': glColor3f(1,0,1);
        break;
    }
    //glColor3ub(rand()%256, rand()%256, rand()%256);
}

// **********************************************************************
//  void display( void )
//
// **********************************************************************
void DesenhaEsfera(int i)
{
    GeraCor(Esferas[i].tipo);
    DesenhaEsfera(Esferas[i].x, Esferas[i].y, Esferas[i].z);
}
// **********************************************************************
//  void display( void )
//
// **********************************************************************
Ponto p(0,0,5), l(0,0,0), u(0,1,0);
float nearP = 1.0f, farP = 100.0f;
float angle = 45, ratio=1;

void display( void )
{

	// Limpa a tela coma cor de fundo
	glClear(GL_COLOR_BUFFER_BIT);

    // Define os limites l�gicos da �rea OpenGL dentro da Janela
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glOrtho(-50,50,-50,50,-300,300);
    gluLookAt(p.x, p.y, p.z, l.x, l.y, l.z, u.x,u.y, u.z);

    frustrum.setCamDef(p,l,u);

    glRotatef(angulo,0,1,0);
    for(int i=0; i<QtdTotal; i++)
    {
        DesenhaEsfera(i);
    }

	glutSwapBuffers();
}


// **********************************************************************
//  void keyboard ( unsigned char key, int x, int y )
//
// **********************************************************************

void keyboard ( unsigned char key, int x, int y )
{
    cout << "Tecla" << endl;
	switch ( key )
	{
		case 27:        // Termina o programa qdo
			exit ( 0 );   // a tecla ESC for pressionada
			break;
        case ' ': angulo++;
                  glutPostRedisplay();
                 break;
		default:
			break;
	}
}


// **********************************************************************
//  void arrow_keys ( int a_keys, int x, int y )
//
//
// **********************************************************************
void arrow_keys ( int a_keys, int x, int y )
{
	switch ( a_keys )
	{
		case GLUT_KEY_UP:       // Se pressionar UP
			glutFullScreen ( ); // Vai para Full Screen
			break;
	    case GLUT_KEY_DOWN:     // Se pressionar UP
								// Reposiciona a janela
            glutPositionWindow (50,50);
			glutReshapeWindow ( 700, 500 );
			break;
		default:
			break;
	}
}

// **********************************************************************
//  void main ( int argc, char** argv )
//
//
// **********************************************************************
int  main ( int argc, char** argv )
{
    glutInit            ( &argc, argv );
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB );
    glutInitWindowPosition (0,0);

    // Define o tamanho inicial da janela grafica do programa
    glutInitWindowSize  ( 650, 500);

    // Cria a janela na tela, definindo o nome da
    // que aparecera na barra de t�tulo da janela.
    glutCreateWindow    ( "Primeiro Programa em OpenGL" );

    // executa algumas inicializa��es
    init ();


    // Define que o tratador de evento para
    // o redesenho da tela. A funcao "display"
    // ser� chamada automaticamente quando
    // for necess�rio redesenhar a janela
    glutDisplayFunc ( display );

    // Define que o tratador de evento para
    // o redimensionamento da janela. A funcao "reshape"
    // ser� chamada automaticamente quando
    // o usu�rio alterar o tamanho da janela
    glutReshapeFunc ( reshape );

    // Define que o tratador de evento para
    // as teclas. A funcao "keyboard"
    // ser� chamada automaticamente sempre
    // o usu�rio pressionar uma tecla comum
    glutKeyboardFunc ( keyboard );

    // Define que o tratador de evento para
    // as teclas especiais(F1, F2,... ALT-A,
    // ALT-B, Teclas de Seta, ...).
    // A funcao "arrow_keys" ser� chamada
    // automaticamente sempre o usu�rio
    // pressionar uma tecla especial
    glutSpecialFunc ( arrow_keys );

    // inicia o tratamento dos eventos
    glutMainLoop ( );

    return 0;
}
